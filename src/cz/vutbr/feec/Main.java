package cz.vutbr.feec;

public class Main {
    public static void main(String[] args) {
        BinarySearchTree bst = new BinarySearchTree();

        bst.insertNode(5);
        bst.insertNode(10);
        bst.insertNode(4);
        bst.insertNode(7);
        bst.insertNode(8);
        bst.insertNode(99);
        bst.printPreorder();

    }
}
