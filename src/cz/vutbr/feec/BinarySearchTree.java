package cz.vutbr.feec;

public class BinarySearchTree {
    private Node root;

    public BinarySearchTree() {
        root = null;
    }

    public void insertNode(int key) {
        Node newNode = new Node(key);
        if(root == null) {
            root = newNode;
        }else {
            Node current = root;
            while(current != null) {
                if(current.getKey() == key) {
                    return;
                } else if(key < current.getKey()) {
                    if(current.getLeft() == null) {
                        current.setLeft(newNode);
                    } else {
                        current = current.getLeft();
                    }
                } else {
                    if(current.getRight() == null) {
                        current.setRight(newNode);
                    } else {
                        current = current.getRight();
                    }
                }
            }
        }
    }



    public void printPreorder() {
        printPreorder(root);
    }

    private void printPreorder(Node node) {
        if (node == null) {
            return;
        }
        System.out.println(node.getKey() + " ");

        printPreorder(node.getLeft());

        printPreorder(node.getRight());
    }


}
